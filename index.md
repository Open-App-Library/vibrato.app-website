---
layout: base
title: "Vibrato Notes: Coming Soon"
---

Coming soon! An extensible open-source note-taking application for all of your devices: Vibrato Notes.

{% include email.html %}

Some of its killer features will include:

* 100% open-source (GPL license)
* Hybrid rich-text/markdown editor
* Seamless cloud-syncing service estimated at $10/month (Or self-host it yourself!)
* Client-side encryption (You're the king of your data!)
* Hackable interface using a Lisp-like scripting language
* Desktop application powered by Qt5
* 5% of revenue goes to other open-source projects
* Available on Linux, Mac, Windows, Android & iOS!

Thanks for taking the time to read this page! Make sure to sign up to our email newsletter to get notified when we launch.

Cheers,<br>
-Doug Beney (Creator of The Open App Library Project)

<ul id="coming-soon-links">
<li>
    <a href="https://openapplibrary.org/projects">Source Code Links</a>
</li>
<li>
    <a href="https://features.vibrato.app">Feature Requests</a>
</li>
<li>
    <a href="https://openapplibrary.org/privacy">Privacy Policy</a>
</li>
</ul>
